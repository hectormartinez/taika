"""
:mod:`taika.ext.markdown` -- Markdown
=====================================

This extension parses the content of the documents into HTML using CommonMarkdown specifications.

Trigger
-------

This extension is subscribed to the "doc-post-read" event.

Frontmatter
-----------

None.

Process
-------

#. Reads the suffix of `path` and if it matches, process the document.
#. Modifies the suffix of `url` path to ".html".
#. Process the content with `marko.convert` and replaces it.
#. Done!

Configuration
-------------

All configuration hangs from a key in the YML configuration named 'markdown'.
Inside it, you can add the following options:

.. data:: suffixes (list)

    Default: **[.md]**

    Tells the parser to ONLY modify docs with that suffix. Otherwise the document is ignored.
    This is checked against the source path (`path`), not the destination path (`url`).

Functions
---------
"""
import logging

import marko

DEFAULT_CONFIG = {"suffixes": [".md"], "options": {}}

LOGGER = logging.getLogger(__name__)


def parse(site, document):
    config = site.config.get("markdown", DEFAULT_CONFIG)
    suffixes = config["suffixes"]
    if document["path"].suffix not in suffixes:
        return

    document["url"] = document["url"].with_suffix(".html")
    try:
        document["content"] = marko.convert(document["content"].decode("utf-8"))
    except AttributeError:
        print(f"Error processing document {document['path']}")
        raise


def setup(site):
    site.events.register("doc-post-read", parse)
