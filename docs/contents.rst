Table of Contents
=================

.. toctree::
   :hidden:

   index

.. toctree::
   :maxdepth: 3

   introduction/index
   how-tos/index
   extensions/index
   internals/index
   reference/index
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
