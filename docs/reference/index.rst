Reference
=========

Take a look here if you want to use Taika's functions or classes yourself.

.. toctree::
   :maxdepth: 2
   :glob:

   documents
   conf
   *
