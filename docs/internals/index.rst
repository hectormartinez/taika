Internals
=========

Take a look here if you want to help improve Taika or want to learn about how Taika is managed.

.. toctree::
   :maxdepth: 2
   :glob:

   *
