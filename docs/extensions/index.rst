Extensions
==========

Taika comes with builtin extensions that will help you to develop better your site.

Take a look at the builtin extensions, or create your own!


.. toctree::
   :maxdepth: 2
   :glob:

   *
