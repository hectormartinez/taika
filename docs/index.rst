Taika Documentation
===================

Taika is a simple Static Content Generator, it can be a simple files processor or can be
more complex, if extensions are used. It aims to be simple and extensible, so if a user
needs more functionality can add it via extensions.

You want a full overview of the documentation? Take a look at the :doc:`contents`.

Getting started
---------------

* :doc:`introduction/installation`
* :doc:`introduction/tutorial/`


How-To Guides
-------------

Take a look at our :doc:`Guides <how-tos/index>` to build more complex things with Taika.


Extensions
----------

Add more functionality to your site with extensions!

Check the builtin extensions. Here are a few of them:

* :doc:`extensions/layouts`
* :doc:`extensions/rst`
* :doc:`extensions/collections`

Or they don't fit your needs, :doc:`create your own extension<how-tos/create-extensions>`.


API reference
-------------

Check the :doc:`configuration reference <reference/conf>`.

How Taika works? Take a look at the :doc:`reference/index`.


Taika itself
------------

How is Taika managed and how you can contribute to it.

:doc:`internals/contributing`
