.. highlight:: shell

============
Installation
============

From PyPI
---------

To install Taika, run in your terminal:

.. code-block:: bash

   pip install taika

You will get the last release available from the PyPI.

From source
-----------

The sources for Taika can be downloaded from the `GitLab repo`_.

Cloning using ``git``:

.. code-block:: bash

   git clone git://gitlab.com/hectormartinez/taika.git

Or downloading a tar:

.. code-block:: bash

   curl -OL https://gitlab.com/hectormartinez/taika/repository/master/archive.tar
   pip install archive.tar

Once you have a copy of the source, you can install it with:

.. code-block:: bash

   cd taika
   pip install .

.. _GitLab repo: https://gitlab.com/hectormartinez/taika
