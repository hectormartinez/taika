---
# An employee record
name: Martin D'vloper
job: Developer
skill: Elite
employed: True
foods:
   - Apple
   - Orange
   - Strawberry
   - Mango
languages:
   perl: Elite
   python: Elite
   pascal: Lame
education: |
   4 GCSEs
   3 A-Levels
   BSc in the Internet of Things
---

Some title
==========

And subtitles too
-----------------

And text.
