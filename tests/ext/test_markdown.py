from pathlib import Path

from taika.events import EventManager
from taika.ext import markdown
from taika.taika import read_file


class FakeSite(object):
    def __init__(self):
        self.events = EventManager()
        self.config = {}
        self.source = Path("../source")


def test_setup():
    site = FakeSite()
    markdown.setup(site)

    assert len(site.events.funcs["doc-post-read"]) == 1


def test_defaults():
    site = FakeSite()
    document = read_file("../source/simple.md")

    markdown.parse(site, document)

    assert document["url"].suffix == ".html"
    assert document["path"].suffix != ".html"
    assert "<p>" in document["content"]


def test_complete_article():
    site = FakeSite()
    document = read_file("../source/markdown.md")

    markdown.parse(site, document)

    assert "<p>" in document["content"]
    assert "<h2>" in document["content"]
