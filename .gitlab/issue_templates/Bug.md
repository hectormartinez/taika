<!--
    Based on: https://gitlab.com/help/user/project/description_templates.md#description-template-example
-->
## Summary

<!--
    Summarize the bug encountered in one or two lines.
-->

## Versions

<!--
    Introduce the versions of the platform, Taika and other things.
-->

**Taika**:

**OS**:

**Python**:

**Other**:

## What is the current bug behavior?

<!--
    What actually happens?

    Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's very hard to read otherwise.
-->

## What is the expected correct behavior?

<!--
    What you should see instead?
-->

## Steps to reproduce

<!--
    How one can reproduce the issue - this is very important.

    PLEASE try to create minimum complete verificable example: https://stackoverflow.com/help/mcve
-->


/label ~"type: bug"
