<!--
    Based on: https://meta.stackexchange.com/questions/258136/how-do-i-write-a-good-feature-request

    Reminder: you are trying to "sell" us your enhancement, so try to sell it properly.

    DO
    --
    * Be specific.
    * Reference content that support your request: discussions, statistics, etc.
    * State what it lacks or what is needs.

    DON'T
    -----
    * Be vague about the benefits or the drawbacks.
    * Use emotions or motivation to support your request. Stay cold.

    EXAMPLE
    -------

    ## Enhancement

    I was filling today an issue in this repository and I used the current "bug" issue template,
    as expected. However, I felt that it lacks certain things. The improvements that I think that
    it needs are:

    * Titles: they will help to layout the informations, as now all of it is a huge block of text.
      They will help to organize the thoughts and the information.

    * Versions section: it doesn't have a section to include software and OS version! I think that
      doesn't need a word about benefits.

    And I will remove:

    * The "Example Project" section: it doesn't make sense here, we won't do a project to reproduce
      the problem.

    Thank you!
-->


/label ~"type: enhancement"
