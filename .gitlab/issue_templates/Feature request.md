<!--
    Based on: https://meta.stackexchange.com/questions/258136/how-do-i-write-a-good-feature-request

    Reminder: you are trying to "sell" us your feature request, so try to sell it properly.

    DO
    --
    * Be specific.
    * Reference content that support your request: discussions, statistics, etc.

    DON'T
    -----
    * Be vague about the benefits or the drawbacks.
    * Use emotions or motivation to support your request. Stay cold.

-->


/label ~"type: feature-request"
