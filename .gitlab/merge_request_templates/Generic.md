<!--
    Based on: https://www.atlassian.com/blog/git/written-unwritten-guide-pull-requests

    Reminder: try to "sell" the merge request to us, if it's difficult to follow, it will
    take more time to merge.

    DO
    --
    * Write a useful title and description
    * Talk about what changed and why changed.
    * If the changes have observable effects, take a picture.

    DON'T
    -----
    * Talk about HOW changed, that is explaining the code diff and we already have the diff.
-->

