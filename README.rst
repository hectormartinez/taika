Taika
=====

.. image:: https://img.shields.io/pypi/v/taika.svg
    :target: https://pypi.python.org/pypi/taika

.. image:: https://readthedocs.org/projects/taika/badge/?version=latest
    :target: https://taika.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://gitlab.com/hectormartinez/taika/badges/master/pipeline.svg
    :target: https://gitlab.com/hectormartinez/taika/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.com/hectormartinez/taika/badges/master/coverage.svg
    :target: https://gitlab.com/hectormartinez/taika/commits/master
    :alt: Coverage Report

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/ambv/black
   :alt: Code style: black


Taika in another Static Site Generator which I created as a learning activity.
Today I use it to build my personal page.

Features
--------

* YAML configuration
* Jinja2 templates

License
-------

`MIT <./LICENSE>`__

