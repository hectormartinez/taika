Contributing to Taika
=====================

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

* Fixing bugs
* Reviewing merge requests
* Writing and improving documentation


You can find a more exhaustive guidelines at the Documentation > Internals.
